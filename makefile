CPP_FILES := $(wildcard src/*.cpp)
OBJ_FILES := $(addprefix obj/,$(notdir $(CPP_FILES:.cpp=.o)))
BINDIR := bin/
OBJDIR := obj/
SRCDIR := src/
CFLAGS = -std=c++11 -O3

main: $(OBJ_FILES)
			mkdir -p $(BINDIR)
			g++ $(CFLAGS) -o $(BINDIR)$@ $^ -lm -lpthread

obj/%.o: src/%.cpp
			mkdir -p $(OBJDIR)
			g++ $(CFLAGS) -c -o $@ $< -lm -lpthread
			
optimized:
	make -f makefile CFLAGS="-O3"

debug: 
	make -f makefile CFLAGS="-g"

clean:
	rm -f $(OBJ_FILES) 
	rm -f $(BINDIR)*~
	rm -f $(OBJDIR)*~
	rm -f $(SRCDIR)*~
	
cleanall:
	rm -f $(OBJ_FILES) $(BINDIR)main
	rm -fr $(BINDIR)
	rm -fr $(OBJDIR)
	
run:
	./bin/main -threads 24 -pctg_access 0.8
