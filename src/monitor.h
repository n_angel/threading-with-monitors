/*
 * DifferentialEvolution.h
 *
 */

// semaphore
#ifndef MONITOR_H
#define MONITOR_H

//// Standard includes. /////////////////////////////////////////////////////
# include <pthread.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <errno.h> 
# include <sys/time.h>

# include <vector>
# include <random>
# include <algorithm>

# define INSERT 0
# define READ 1

//# define MAX_THREADS 24
//// New includes. /////////////////////////////////////////////////////

typedef struct {
    int w_active;
    int w_wait;
    pthread_cond_t w_cond;
    int r_active;
    int r_wait;
    pthread_cond_t r_cond;
    pthread_mutex_t read_write_lock;
} mylib_rwlock_t;
 

void mylib_rwlock_init (mylib_rwlock_t *l);
void mylib_rwlock_rlock(mylib_rwlock_t *l);
void mylib_rwlock_runlock(mylib_rwlock_t *l);
void mylib_rwlock_wlock(mylib_rwlock_t *l);
void mylib_rwlock_wunlock(mylib_rwlock_t *l);

class my_hash
{
private:
    std::vector< std::vector<int> > hash;
public:
	my_hash();
	void insert(int data);
	bool isInserted(int data);
};

# endif
