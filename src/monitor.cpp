/*
 * parameter_handler.cpp
 *
 * Implementation of parameter_handler.h
 */

# include "monitor.h"

//// Standard includes. /////////////////////////////////////////////////////

//// Used namespaces. ///////////////////////////////////////////////////////
//using namespace PARAMETER;

 //// New includes. /////////////////////////////////////////////////////

//// Implemented functions. /////////////////////////////////////////////////

 
void mylib_rwlock_init (mylib_rwlock_t *l)
{
    l->w_active = l->w_wait = l->r_active = l->r_wait = 0;
    pthread_mutex_init(&(l->read_write_lock), NULL);
    pthread_cond_init(&(l->w_cond), NULL);
    pthread_cond_init(&(l->r_cond), NULL);
}
 
void mylib_rwlock_rlock(mylib_rwlock_t *l)
{
    pthread_mutex_lock(&(l->read_write_lock));
    if(l->w_active > 0 || l->w_wait > 0)
    {
        l->r_wait++;
        while(l->w_active > 0 || l->w_wait > 0)
        {
            pthread_cond_wait(&(l->r_cond), &(l->read_write_lock));
        }
        l->r_wait --;
    }
    l->r_active += 1;
    pthread_mutex_unlock(&(l->read_write_lock));
}

void mylib_rwlock_runlock(mylib_rwlock_t *l)
{
    pthread_mutex_lock(&(l->read_write_lock));
    l->r_active --;
    pthread_cond_signal(&l->w_cond);
    pthread_mutex_unlock(&(l->read_write_lock));
}
 
void mylib_rwlock_wlock(mylib_rwlock_t *l)
{
    pthread_mutex_lock(&(l->read_write_lock));
    if(l->w_active == 1 || l->r_active > 0)
    {
        l->w_wait++;
        while(l->w_active == 1 || l->r_active > 0)
        {
            pthread_cond_wait(&l->w_cond, &(l->read_write_lock));
        }
        l->w_wait--;
    }
    l->w_active = 1;
    pthread_mutex_unlock(&(l->read_write_lock));
}
 
void mylib_rwlock_wunlock(mylib_rwlock_t *l)
{
    pthread_mutex_lock(&(l->read_write_lock));
    l->w_active = 0;
    pthread_cond_signal(&l->w_cond);
    pthread_cond_broadcast(&l->r_cond);
    pthread_mutex_unlock(&(l->read_write_lock));
}

//// ********************************
//// Hash class
//// ********************************

my_hash::my_hash()
{
    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(0,100);

    this->hash = std::vector< std::vector< int > > (10, std::vector< int >(0));

    for (int i = 0; i < 100000; ++i)
    {
        int rnd = i;//distribution(generator);
        this->hash[rnd%10].push_back(rnd);
    }
}

void my_hash::insert(int data)
{
    this->hash[data%10].push_back(data);
}

bool my_hash::isInserted(int data)
{
    std::vector<int>::iterator itr;
    itr = std::find(this->hash[data%10].begin(), this->hash[data%10].end(), data);

    return itr != this->hash[data%10].end();
}
