// standard libraries
# include <iostream>
# include <cstdio>
# include <vector>
# include <random>
# include <algorithm>

// new libraries
# include "parameter_handler.h"
# include "monitor.h"

# define ACCESOS 1000000

// global vars
std::default_random_engine generator;
std::vector<int> opts;
my_hash hash;
int elements_per_thread;
int ptr;

// threads global vars
void *rw_test(void *s);
std::vector<pthread_t> p_threads;
mylib_rwlock_t rwl;

// Main
int main(int argc, char *argv[])
{
    std::map<std::string, std::string> params = PARAMETER::unpack_parameters(argc, argv);
    double pctg_insert = std::stof(PARAMETER::get_value(params, "-pctg_access"));
    int MAX_THREADS = std::stoi(PARAMETER::get_value(params, "-threads"));

    int i, hits[MAX_THREADS];
    struct timeval tim;
    ptr = 100001;

    // Crea la lista de operaciones a realizar
    opts = std::vector<int>((int)(ACCESOS * pctg_insert), INSERT);  // Operaciones de inserción a hash
    std::vector<int> opt_read((int)ACCESOS * (1.0 - pctg_insert), READ);    // Operaciones de pregunta
    opts.insert(opts.end(), opt_read.begin(), opt_read.end());
    std::random_shuffle (opts.begin(), opts.end()); // Orden aleatorio para realizarlas

    elements_per_thread = (double)opts.size()/ (double)MAX_THREADS;

    mylib_rwlock_init (&rwl);

    // Check starting time
    gettimeofday(&tim, NULL);
    double t1=tim.tv_sec+(tim.tv_usec/1000000.0);

    p_threads = std::vector<pthread_t>(MAX_THREADS);
    for (i = 0; i < MAX_THREADS; i ++)
    {
        hits[i] = i;
        pthread_create(&p_threads[i], NULL, rw_test, (void *) &hits[i]);
    }
    for (i = 0; i < MAX_THREADS; i ++)
    {
        pthread_join(p_threads[i], NULL);
    }

    gettimeofday(&tim, NULL);
    double t2=tim.tv_sec+(tim.tv_usec/1000000.0);
    printf("Avg time = %lf sec\n", (t2-t1));
    
    return 0;
}

// Thread function
void *rw_test (void *start_ptr)
{
    std::uniform_int_distribution<int> distribution(1, ACCESOS);
    int my_id, i, my_min;

    my_id = *((int *) start_ptr);

    int it_i;
    int ctr = 0;
    for (it_i = my_id * elements_per_thread; it_i < (my_id + 1) * elements_per_thread; it_i++)
    {
        //printf("%d: operacion %d de %d\n", my_id, ctr, elements_per_thread);
        if (opts[it_i] == INSERT)
        {
            mylib_rwlock_wlock(&rwl);
            hash.insert(ptr);
            ptr ++;
            mylib_rwlock_wunlock(&rwl);
        }
        else  if (opts[it_i] == READ)
        {
            mylib_rwlock_rlock(&rwl);
            int dt = distribution(generator);
            bool check = hash.isInserted(ptr);
            mylib_rwlock_runlock(&rwl);
        }
        ctr ++;
    }

    return NULL;
}

